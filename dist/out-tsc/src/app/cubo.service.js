"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var cara_1 = require("./cara");
var cubo_1 = require("./cubo");
var movimiento_1 = require("./movimiento");
var CuboService = /** @class */ (function () {
    function CuboService() {
        this.cubo = this.llenarLista("");
        this.listaMovimiento = this.readMovimiento();
    }
    CuboService.prototype.derecha = function (fila) {
        var row = fila - 1;
        var uno = this.cubo.caras[3].celdas[row];
        var dos = this.cubo.caras[0].celdas[row];
        var tres = this.cubo.caras[1].celdas[row];
        var cuatro = this.cubo.caras[2].celdas[row];
        this.cubo.caras[0].celdas[row] = uno;
        this.cubo.caras[1].celdas[row] = dos;
        this.cubo.caras[2].celdas[row] = tres;
        this.cubo.caras[3].celdas[row] = cuatro;
        if (fila == 1) {
            this.rotarCara(4);
        }
        else if (fila == 3) {
            for (var i = 0; i < 3; ++i) {
                this.rotarCara(5);
            }
        }
        this.updateCara();
        this.saveCubo();
    };
    CuboService.prototype.izquierda = function (row) {
        for (var i = 0; i < 3; ++i) {
            this.derecha(row);
        }
    };
    CuboService.prototype.abajo = function (column) {
        for (var i = 0; i < 3; ++i) {
            this.arriba(column);
        }
    };
    CuboService.prototype.arriba = function (column) {
        var col = column - 1;
        var col2 = 0;
        if (col == 0) {
            col2 = 2;
        }
        else if (col == 1) {
            col2 = 1;
        }
        var uno = [];
        uno.push(this.cubo.caras[5].celdas[0][col]);
        uno.push(this.cubo.caras[5].celdas[1][col]);
        uno.push(this.cubo.caras[5].celdas[2][col]);
        var dos = [];
        dos.push(this.cubo.caras[0].celdas[0][col]);
        dos.push(this.cubo.caras[0].celdas[1][col]);
        dos.push(this.cubo.caras[0].celdas[2][col]);
        var tres = [];
        tres.push(this.cubo.caras[4].celdas[2][col]);
        tres.push(this.cubo.caras[4].celdas[1][col]);
        tres.push(this.cubo.caras[4].celdas[0][col]);
        var cuatro = [];
        cuatro.push(this.cubo.caras[2].celdas[2][col2]);
        cuatro.push(this.cubo.caras[2].celdas[1][col2]);
        cuatro.push(this.cubo.caras[2].celdas[0][col2]);
        this.cubo.caras[0].celdas[0][col] = uno[0];
        this.cubo.caras[0].celdas[1][col] = uno[1];
        this.cubo.caras[0].celdas[2][col] = uno[2];
        this.cubo.caras[4].celdas[0][col] = dos[0];
        this.cubo.caras[4].celdas[1][col] = dos[1];
        this.cubo.caras[4].celdas[2][col] = dos[2];
        this.cubo.caras[2].celdas[0][col2] = tres[0];
        this.cubo.caras[2].celdas[1][col2] = tres[1];
        this.cubo.caras[2].celdas[2][col2] = tres[2];
        this.cubo.caras[5].celdas[0][col] = cuatro[0];
        this.cubo.caras[5].celdas[1][col] = cuatro[1];
        this.cubo.caras[5].celdas[2][col] = cuatro[2];
        if (column == 1) {
            this.rotarCara(3);
        }
        else if (column == 3) {
            for (var i = 0; i < 3; ++i) {
                this.rotarCara(1);
            }
        }
        this.updateCara();
        this.saveCubo();
    };
    CuboService.prototype.abajoVerde = function (column) {
        for (var i = 0; i < 3; ++i) {
            this.arribaVerde(column);
        }
    };
    CuboService.prototype.arribaVerde = function (column) {
        var col = 0;
        var col2 = 2;
        if (column == 2) {
            col = 1;
            col2 = 1;
        }
        else if (column == 3) {
            col = 2;
            col2 = 0;
        }
        var uno = this.cubo.caras[5].celdas[col2];
        var dos = [];
        dos.push(this.cubo.caras[3].celdas[2][col]);
        dos.push(this.cubo.caras[3].celdas[1][col]);
        dos.push(this.cubo.caras[3].celdas[0][col]);
        var tres = this.cubo.caras[4].celdas[col];
        var cuatro = [];
        cuatro.push(this.cubo.caras[1].celdas[2][col2]);
        cuatro.push(this.cubo.caras[1].celdas[1][col2]);
        cuatro.push(this.cubo.caras[1].celdas[0][col2]);
        this.cubo.caras[3].celdas[0][col] = uno[0];
        this.cubo.caras[3].celdas[1][col] = uno[1];
        this.cubo.caras[3].celdas[2][col] = uno[2];
        this.cubo.caras[4].celdas[col] = dos;
        this.cubo.caras[1].celdas[0][col2] = tres[0];
        this.cubo.caras[1].celdas[1][col2] = tres[1];
        this.cubo.caras[1].celdas[2][col2] = tres[2];
        this.cubo.caras[5].celdas[col2] = cuatro;
        if (column === 1) {
            this.rotarCara(2);
        }
        else if (column === 3) {
            for (var i = 0; i < 3; ++i) {
                this.rotarCara(0);
            }
        }
        this.updateCara();
        this.saveCubo();
    };
    CuboService.prototype.rotarCara = function (cara) {
        var uno = [];
        uno.push(this.cubo.caras[cara].celdas[0][2]);
        uno.push(this.cubo.caras[cara].celdas[1][2]);
        uno.push(this.cubo.caras[cara].celdas[2][2]);
        var dos = this.cubo.caras[cara].celdas[0];
        var tres = [];
        tres.push(this.cubo.caras[cara].celdas[0][0]);
        tres.push(this.cubo.caras[cara].celdas[1][0]);
        tres.push(this.cubo.caras[cara].celdas[2][0]);
        var cuatro = this.cubo.caras[cara].celdas[2];
        this.cubo.caras[cara].celdas[0] = uno;
        this.cubo.caras[cara].celdas[1][0] = dos[1];
        this.cubo.caras[cara].celdas[2] = tres;
        this.cubo.caras[cara].celdas[1][2] = cuatro[1];
    };
    CuboService.prototype.llenarLista = function (funcion) {
        if (localStorage.getItem('cubo') === null || funcion === "reset") {
            var listaCaras = [];
            var caraFrente = new cara_1.Cara("rgba(255, 255, 0, 0.9)", "amarilla");
            listaCaras.push(caraFrente);
            var caraDerecha = new cara_1.Cara("rgba(51, 51, 204, 0.9)", "azul");
            listaCaras.push(caraDerecha);
            var caraFondo = new cara_1.Cara("rgba(255, 255, 255, 0.9)", "blanca");
            listaCaras.push(caraFondo);
            var caraIzquierda = new cara_1.Cara("rgba(0, 153, 51, 0.9)", "verde");
            listaCaras.push(caraIzquierda);
            var caraArriba = new cara_1.Cara("rgba(255, 0, 0, 0.9)", "roja");
            listaCaras.push(caraArriba);
            var caraAbajo = new cara_1.Cara("rgba(255, 102, 0, 0.9)", "naranja");
            listaCaras.push(caraAbajo);
            var cuboTemp = new cubo_1.Cubo(listaCaras);
            localStorage.setItem('cubo', JSON.stringify(cuboTemp));
            return cuboTemp;
        }
        else {
            return JSON.parse(localStorage.getItem('cubo') || '[]');
        }
    };
    CuboService.prototype.saveCubo = function () {
        localStorage.setItem('cubo', JSON.stringify(this.cubo));
    };
    CuboService.prototype.cuboRead = function () {
        return this.cubo;
    };
    CuboService.prototype.validar = function () {
        for (var i = 0; i < this.cubo.caras.length; ++i) {
            var color = "";
            for (var j = 0; j < this.cubo.caras[i].celdas.length; ++j) {
                for (var k = 0; k < this.cubo.caras[i].celdas[j].length; ++k) {
                    if (color == "") {
                        color = this.cubo.caras[i].celdas[j][k].color;
                    }
                    if (color != this.cubo.caras[i].celdas[j][k].color) {
                        return "Incompleto";
                    }
                    color = this.cubo.caras[i].celdas[j][k].color;
                }
            }
        }
        return "Completo";
    };
    CuboService.prototype.resetCubo = function () {
        this.cubo = this.llenarLista("reset");
        this.resetMovimiento();
    };
    CuboService.prototype.resetMovimiento = function () {
        this.listaMovimiento = [];
        localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
    };
    CuboService.prototype.guardarMovimiento = function (dir, pos) {
        var move = new movimiento_1.Movimiento(dir, pos);
        this.listaMovimiento.push(move);
        localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
    };
    CuboService.prototype.readMovimiento = function () {
        this.listaMovimiento = JSON.parse(localStorage.getItem('movimiento') || '[]');
        return this.listaMovimiento;
    };
    CuboService.prototype.backMove = function () {
        debugger;
        if (this.listaMovimiento.length > 0) {
            var move = this.listaMovimiento[this.listaMovimiento.length - 1];
            if (move.direccion == "arriba") {
                this.arriba(move.posicion);
            }
            else if (move.direccion == "abajo") {
                this.abajo(move.posicion);
            }
            else if (move.direccion == "izquierda") {
                this.izquierda(move.posicion);
            }
            else if (move.direccion == "arribaVerde") {
                this.arribaVerde(move.posicion);
            }
            else if (move.direccion == "abajoVerde") {
                this.abajoVerde(move.posicion);
            }
            else {
                this.derecha(move.posicion);
            }
            this.listaMovimiento.splice(this.listaMovimiento.length - 1, 1);
            localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
        }
    };
    CuboService.prototype.updateCara = function () {
        for (var i = 0; i < this.cubo.caras.length; ++i) {
            for (var j = 0; j < this.cubo.caras[i].celdas.length; ++j) {
                for (var k = 0; k < this.cubo.caras[i].celdas[j].length; ++k) {
                    if (i === 0) {
                        this.cubo.caras[i].celdas[j][k].cara = "amarilla";
                    }
                    else if (i === 1) {
                        this.cubo.caras[i].celdas[j][k].cara = "azul";
                    }
                    else if (i === 2) {
                        this.cubo.caras[i].celdas[j][k].cara = "blanca";
                    }
                    else if (i === 3) {
                        this.cubo.caras[i].celdas[j][k].cara = "verde";
                    }
                    else if (i === 4) {
                        this.cubo.caras[i].celdas[j][k].cara = "roja";
                    }
                    else if (i === 5) {
                        this.cubo.caras[i].celdas[j][k].cara = "naranja";
                    }
                }
            }
        }
    };
    CuboService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], CuboService);
    return CuboService;
}());
exports.CuboService = CuboService;
//# sourceMappingURL=cubo.service.js.map