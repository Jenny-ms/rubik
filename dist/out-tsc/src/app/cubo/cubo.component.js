"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var cubo_service_1 = require("../cubo.service");
var CuboComponent = /** @class */ (function () {
    function CuboComponent(service) {
        this.service = service;
        this.rotateY = 45;
        this.rotateX = -20;
        this.column = 1;
        this.row = 1;
        this.cubo = this.service.cuboRead();
        this.listaMovimiento = this.service.readMovimiento();
    }
    CuboComponent.prototype.ngOnInit = function () {
    };
    CuboComponent.prototype.derecha = function () {
        this.service.derecha(this.row);
        this.service.guardarMovimiento("izquierda", this.row);
    };
    CuboComponent.prototype.izquierda = function () {
        this.service.izquierda(this.row);
        this.service.guardarMovimiento("izquierda", this.row);
    };
    CuboComponent.prototype.abajo = function () {
        this.service.abajo(this.column);
    };
    CuboComponent.prototype.arriba = function () {
        this.service.arriba(this.column);
        this.service.guardarMovimiento("abajo", this.column);
    };
    CuboComponent.prototype.rotarCara = function (cara) {
        this.service.rotarCara(cara);
    };
    CuboComponent.prototype.back = function () {
        this.service.backMove();
        this.service.saveCubo();
        this.cubo = this.service.cuboRead();
    };
    CuboComponent.prototype.mess = function () {
        var cont = 0;
        while (cont < 20) {
            var x = Math.floor(Math.random() * 4) + 1;
            this.row = Math.floor(Math.random() * 3) + 1;
            this.column = Math.floor(Math.random() * 3) + 1;
            if (x === 1) {
                this.derecha();
            }
            else if (x === 2) {
                this.izquierda();
            }
            else if (x === 3) {
                this.arriba();
            }
            else if (x === 4) {
                this.abajo();
            }
            cont++;
        }
        this.column = 1;
        this.row = 1;
    };
    CuboComponent.prototype.moverCubo = function (event) {
        if (event.keyCode === 37) {
            this.rotateY -= 2;
        }
        else if (event.keyCode === 39) {
            this.rotateY += 2;
        }
        else if (event.keyCode === 38) {
            this.rotateX += 2;
        }
        else if (event.keyCode === 40) {
            this.rotateX -= 2;
        }
    };
    CuboComponent.prototype.rotate = function () {
        return 'rotateX(' + this.rotateX + 'deg) rotateY(' + this.rotateY + 'deg)';
    };
    CuboComponent.prototype.reset = function () {
        this.service.resetCubo();
        this.cubo = this.service.cuboRead();
        this.listaMovimiento = this.service.readMovimiento();
        this.rotateY = 45;
        this.rotateX = -20;
    };
    CuboComponent.prototype.validar = function () {
        return this.service.validar();
    };
    CuboComponent.prototype.validarNum = function (event) {
        if ((event.keyCode === 49 || event.keyCode === 50 || event.keyCode === 51)
            && (String(this.column) === "null" || String(this.row) === "null")) {
            return true;
        }
        return false;
    };
    CuboComponent = __decorate([
        core_1.Component({
            selector: 'app-cubo',
            templateUrl: './cubo.component.html',
            styleUrls: ['./cubo.component.css']
        }),
        __metadata("design:paramtypes", [cubo_service_1.CuboService])
    ], CuboComponent);
    return CuboComponent;
}());
exports.CuboComponent = CuboComponent;
//# sourceMappingURL=cubo.component.js.map