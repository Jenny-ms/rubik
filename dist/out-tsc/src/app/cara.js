"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var celda_1 = require("./celda");
var Cara = /** @class */ (function () {
    function Cara(color, cara) {
        this.celdas = [];
        for (var i = 0; i < 3; ++i) {
            this.celdas[i] = [];
            for (var j = 0; j < 3; ++j) {
                this.celdas[i][j] = new celda_1.Celda(color, cara);
            }
        }
    }
    return Cara;
}());
exports.Cara = Cara;
//# sourceMappingURL=cara.js.map