"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Movimiento = /** @class */ (function () {
    function Movimiento(direccion, posicion) {
        this.direccion = direccion;
        this.posicion = posicion;
    }
    return Movimiento;
}());
exports.Movimiento = Movimiento;
//# sourceMappingURL=movimiento.js.map