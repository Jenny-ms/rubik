"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var cubo_service_1 = require("../cubo.service");
var CeldaComponent = /** @class */ (function () {
    function CeldaComponent(service) {
        this.service = service;
        this.lista = [];
        this.flat = false;
    }
    CeldaComponent.prototype.ngOnInit = function () {
    };
    CeldaComponent.prototype.down = function () {
        this.flat = true;
    };
    CeldaComponent.prototype.up = function () {
        this.flat = false;
        this.lista = [];
    };
    CeldaComponent.prototype.move = function (event) {
        var cara = event.currentTarget.className;
        if (this.flat) {
            var x = Number(event.currentTarget.id);
            if ((this.lista.length <= 0 || this.lista[this.lista.length - 1] != x)
                && this.lista.length < 3) {
                this.lista.push(x);
            }
            this.validar(cara);
        }
    };
    CeldaComponent.prototype.validar = function (cara) {
        if (this.lista.length === 3) {
            if (cara === "verde") {
                this.verde();
            }
            else if (cara === "amarilla") {
                this.amarilla();
            }
            else if (cara === "roja") {
                this.roja();
            }
            else if (cara === "azul") {
                this.azul();
            }
            else if (cara === "blanca") {
                this.blanca();
            }
            else if (cara === "naranja") {
                this.naranja();
            }
            this.lista = [];
            this.flat = false;
        }
    };
    CeldaComponent.prototype.verde = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.derecha(1);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.derecha(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.derecha(3);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.izquierda(1);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.izquierda(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.izquierda(3);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.arribaVerde(1);
            this.service.guardarMovimiento("abajoVerde", 1);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.arribaVerde(2);
            this.service.guardarMovimiento("abajoVerde", 2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.arribaVerde(3);
            this.service.guardarMovimiento("abajoVerde", 3);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.abajoVerde(1);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.abajoVerde(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.abajoVerde(3);
        }
    };
    CeldaComponent.prototype.amarilla = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.derecha(1);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.derecha(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.derecha(3);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.izquierda(1);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.izquierda(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.izquierda(3);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.arriba(1);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.arriba(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.arriba(3);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.abajo(1);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.abajo(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.abajo(3);
        }
    };
    CeldaComponent.prototype.roja = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.arribaVerde(1);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.arribaVerde(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.arribaVerde(3);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.abajoVerde(1);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.abajoVerde(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.abajoVerde(3);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.arriba(1);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.arriba(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.arriba(3);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.abajo(1);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.abajo(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.abajo(3);
        }
    };
    CeldaComponent.prototype.azul = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.derecha(1);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.derecha(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.derecha(3);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.izquierda(1);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.izquierda(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.izquierda(3);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.abajoVerde(3);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.abajoVerde(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.abajoVerde(1);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.arribaVerde(3);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.arribaVerde(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.arribaVerde(1);
        }
    };
    CeldaComponent.prototype.blanca = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.derecha(1);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.derecha(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.derecha(3);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.izquierda(1);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.izquierda(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.izquierda(3);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.abajo(3);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.abajo(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.abajo(1);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.arriba(3);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.arriba(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.arriba(1);
        }
    };
    CeldaComponent.prototype.naranja = function () {
        if (this.lista[0] === 1 && this.lista[2] === 3) {
            this.service.abajoVerde(3);
        }
        else if (this.lista[0] === 4 && this.lista[2] === 6) {
            this.service.abajoVerde(2);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 9) {
            this.service.abajoVerde(1);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 1) {
            this.service.arribaVerde(3);
        }
        else if (this.lista[0] === 6 && this.lista[2] === 4) {
            this.service.abajoVerde(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 7) {
            this.service.abajoVerde(1);
        }
        else if (this.lista[0] === 7 && this.lista[2] === 1) {
            this.service.arriba(1);
        }
        else if (this.lista[0] === 8 && this.lista[2] === 2) {
            this.service.arriba(2);
        }
        else if (this.lista[0] === 9 && this.lista[2] === 3) {
            this.service.arriba(3);
        }
        else if (this.lista[0] === 1 && this.lista[2] === 7) {
            this.service.abajo(1);
        }
        else if (this.lista[0] === 2 && this.lista[2] === 8) {
            this.service.abajo(2);
        }
        else if (this.lista[0] === 3 && this.lista[2] === 9) {
            this.service.abajo(3);
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], CeldaComponent.prototype, "celda", void 0);
    CeldaComponent = __decorate([
        core_1.Component({
            selector: 'app-celda',
            templateUrl: './celda.component.html',
            styleUrls: ['./celda.component.css']
        }),
        __metadata("design:paramtypes", [cubo_service_1.CuboService])
    ], CeldaComponent);
    return CeldaComponent;
}());
exports.CeldaComponent = CeldaComponent;
//# sourceMappingURL=celda.component.js.map