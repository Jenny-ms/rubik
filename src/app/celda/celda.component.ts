import { Component, OnInit, Input} from '@angular/core';
import { Celda } from '../celda';
import { CuboService } from '../cubo.service';
import { Sides } from '../sides.enum';
@Component({
  selector: 'app-celda',
  templateUrl: './celda.component.html',
  styleUrls: ['./celda.component.css']
})
export class CeldaComponent implements OnInit {

	@Input() celda: Celda[][];
	private lista : number[];
	private flat : boolean;
  constructor(private service: CuboService) {
  	this.lista = [];
  	this.flat = false;
  }

  ngOnInit() {
  }

  down() {
  	this.flat = true;
  }
  up() {
	this.flat = false;
	this.lista = [];
  }

  move(event) {
  	let cara = event.currentTarget.className;
  	event.preventDefault();
  	if(this.flat) {
  		let x = Number(event.currentTarget.id);
	  	if ((this.lista.length <= 0 || this.lista[this.lista.length-1] != x) 
	  		&& this.lista.length < 3) {
	  		this.lista.push(x);
	  	}
	  	this.validar(cara);
  	}
  }
  validar(cara : string) {
  	if(this.lista.length === 3) {
		if(cara === Sides.Left){
			this.verde();
		}
		else if(cara === Sides.Front){
			this.amarilla();
		}
		else if(cara === Sides.Top){
			this.roja();
		}
		else if(cara === Sides.Right){
			this.azul();
		}
		else if(cara === Sides.Back){
			this.blanca();
		}
		else if(cara === Sides.Bottom){
			this.naranja();
		}
		this.lista = [];
		this.flat = false;
	}
  }
  verde(){
  	if(this.derechaIzquierda()){
  		return;
  	}
	else if (this.lista[0] === 7 && this.lista[2] === 1) {
		this.service.arribaVerde(1);
		this.service.guardarMovimiento("abajoVerde", 1);
	}
	else if (this.lista[0] === 8 && this.lista[2] === 2) {
		this.service.arribaVerde(2);
		this.service.guardarMovimiento("abajoVerde", 2);
	}
	else if (this.lista[0] === 9 && this.lista[2] === 3) {
		this.service.arribaVerde(3);
		this.service.guardarMovimiento("abajoVerde", 3);
	}
	else if (this.lista[0] === 1 && this.lista[2] === 7) {
		this.service.abajoVerde(1);
		this.service.guardarMovimiento("arribaVerde", 1);
	}
	else if (this.lista[0] === 2 && this.lista[2] === 8) {
		this.service.abajoVerde(2);
		this.service.guardarMovimiento("arribaVerde", 2);
	}
	else if (this.lista[0] === 3 && this.lista[2] === 9) {
		this.service.abajoVerde(3);
		this.service.guardarMovimiento("arribaVerde", 3);
	}
  }
  amarilla() {
  	if(this.derechaIzquierda()){
  		return;
  	}
	else if(this.arribaAbajo()){
		return;
	}
  }
  roja(){
  	if(this.lista[0] === 1 && this.lista[2] === 3){
		this.service.arribaVerde(1);
		this.service.guardarMovimiento("abajoVerde", 1);
	}
	else if(this.lista[0] === 4 && this.lista[2] === 6){
		this.service.arribaVerde(2);
		this.service.guardarMovimiento("abajoVerde", 2);
	}
	else if(this.lista[0] === 7 && this.lista[2] === 9){
		this.service.arribaVerde(3);
		this.service.guardarMovimiento("abajoVerde", 3);
	}
	else if(this.lista[0] === 3 && this.lista[2] === 1){
		this.service.abajoVerde(1);
		this.service.guardarMovimiento("arribaVerde", 1);
	}
	else if(this.lista[0] === 6 && this.lista[2] === 4){
		this.service.abajoVerde(2);
		this.service.guardarMovimiento("arribaVerde", 2);
	}
	else if(this.lista[0] === 9 && this.lista[2] === 7){
		this.service.abajoVerde(3);
		this.service.guardarMovimiento("arribaVerde", 3);
	}
	else if(this.arribaAbajo()){
		return;
	}
  }
  azul(){
  	if(this.derechaIzquierda()){
  		return;
  	}
	else if (this.lista[0] === 7 && this.lista[2] === 1) {
		this.service.abajoVerde(3);
		this.service.guardarMovimiento("arribaVerde", 3);
	}
	else if (this.lista[0] === 8 && this.lista[2] === 2) {
		this.service.abajoVerde(2);
		this.service.guardarMovimiento("arribaVerde", 2);
	}
	else if (this.lista[0] === 9 && this.lista[2] === 3) {
		this.service.abajoVerde(1);
		this.service.guardarMovimiento("arribaVerde", 1);
	}
	else if (this.lista[0] === 1 && this.lista[2] === 7) {
		this.service.arribaVerde(3);
		this.service.guardarMovimiento("abajoVerde", 3);
	}
	else if (this.lista[0] === 2 && this.lista[2] === 8) {
		this.service.arribaVerde(2);
		this.service.guardarMovimiento("abajoVerde", 2);
	}
	else if (this.lista[0] === 3 && this.lista[2] === 9) {
		this.service.arribaVerde(1);
		this.service.guardarMovimiento("abajoVerde", 1);
	}
  }
  blanca() {
  	if(this.derechaIzquierda()){
  		return;
  	}
	else if (this.lista[0] === 7 && this.lista[2] === 1) {
		this.service.abajo(3);
		this.service.guardarMovimiento("arriba", 3);
	}
	else if (this.lista[0] === 8 && this.lista[2] === 2) {
		this.service.abajo(2);
		this.service.guardarMovimiento("arriba", 2);
	}
	else if (this.lista[0] === 9 && this.lista[2] === 3) {
		this.service.abajo(1);
		this.service.guardarMovimiento("arriba", 1);
	}
	else if (this.lista[0] === 1 && this.lista[2] === 7) {
		this.service.arriba(3);
		this.service.guardarMovimiento("abajo", 3);
	}
	else if (this.lista[0] === 2 && this.lista[2] === 8) {
		this.service.arriba(2);
		this.service.guardarMovimiento("abajo", 2);
	}
	else if (this.lista[0] === 3 && this.lista[2] === 9) {
		this.service.arriba(1);
		this.service.guardarMovimiento("abajo", 1);
	}
  }
  naranja() {
  	if(this.lista[0] === 1 && this.lista[2] === 3){
		this.service.abajoVerde(3);
		this.service.guardarMovimiento("arribaVerde", 3);
	}
	else if(this.lista[0] === 4 && this.lista[2] === 6){
		this.service.abajoVerde(2);
		this.service.guardarMovimiento("arribaVerde", 2);
	}
	else if(this.lista[0] === 7 && this.lista[2] === 9){
		this.service.abajoVerde(1);
		this.service.guardarMovimiento("arribaVerde", 1);
	}
	else if(this.lista[0] === 3 && this.lista[2] === 1){
		this.service.arribaVerde(3);
		this.service.guardarMovimiento("abajoVerde", 3);
	}
	else if(this.lista[0] === 6 && this.lista[2] === 4){
		this.service.arribaVerde(2);
		this.service.guardarMovimiento("arribaVerde", 2);
	}
	else if(this.lista[0] === 9 && this.lista[2] === 7){
		this.service.arribaVerde(1);
		this.service.guardarMovimiento("arribaVerde", 1);
	}
	else if(this.arribaAbajo()){
		return;
	}
  }

  derechaIzquierda(){
  	if(this.lista[0] === 1 && this.lista[2] === 3){
		this.service.derecha(1);
		this.service.guardarMovimiento("izquierda", 1);
		return true;
	}
	else if(this.lista[0] === 4 && this.lista[2] === 6){
		this.service.derecha(2);
		this.service.guardarMovimiento("izquierda", 2);
		return true;
	}
	else if(this.lista[0] === 7 && this.lista[2] === 9){
		this.service.derecha(3);
		this.service.guardarMovimiento("izquierda", 3);
		return true;
	}
	else if(this.lista[0] === 3 && this.lista[2] === 1){
		this.service.izquierda(1);
		this.service.guardarMovimiento("derecha", 1);
		return true;
	}
	else if(this.lista[0] === 6 && this.lista[2] === 4){
		this.service.izquierda(2);
		this.service.guardarMovimiento("derecha", 2);
		return true;
	}
	else if(this.lista[0] === 9 && this.lista[2] === 7){
		this.service.izquierda(3);
		this.service.guardarMovimiento("derecha", 3);
		return true;
	}
	return false;
  }

  arribaAbajo(){
  	if (this.lista[0] === 7 && this.lista[2] === 1) {
		this.service.arriba(1);
		this.service.guardarMovimiento("abajo", 1);
		return true;
	}
	else if (this.lista[0] === 8 && this.lista[2] === 2) {
		this.service.arriba(2);
		this.service.guardarMovimiento("abajo", 2);
		return true;
	}
	else if (this.lista[0] === 9 && this.lista[2] === 3) {
		this.service.arriba(3);
		this.service.guardarMovimiento("abajo", 3);
		return true;
	}
	else if (this.lista[0] === 1 && this.lista[2] === 7) {
		this.service.abajo(1);
		this.service.guardarMovimiento("arriba", 1);
		return true;
	}
	else if (this.lista[0] === 2 && this.lista[2] === 8) {
		this.service.abajo(2);
		this.service.guardarMovimiento("arriba", 2);
		return true;
	}
	else if (this.lista[0] === 3 && this.lista[2] === 9) {
		this.service.abajo(3);
		this.service.guardarMovimiento("arriba", 3);
		return true;
	}
	return false;
  }
}
