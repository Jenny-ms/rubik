import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CaraComponent } from './cara/cara.component';
import { CuboComponent } from './cubo/cubo.component';
import { CeldaComponent } from './celda/celda.component';

@NgModule({
  declarations: [
    AppComponent,
    CaraComponent,
    CuboComponent,
    CeldaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
