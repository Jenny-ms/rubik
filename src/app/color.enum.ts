export enum Color {
	Red = "rgba(255, 0, 0, 0.9)",
	Yellow = "rgba(255, 255, 0, 0.9)",
	Orange = "rgba(255, 102, 0, 0.9)",
	Blue = "rgba(51, 51, 204, 0.9)",
	White = "rgba(255, 255, 255, 0.9)",
	Green = "rgba(0, 153, 51, 0.9)"
}
