import { Component, OnInit, HostListener} from '@angular/core';
import { Cara } from '../cara';
import { Celda } from '../celda';
import { Cubo } from '../cubo';
import { Movimiento } from '../movimiento';
import { CuboService } from '../cubo.service';
import { Posicion } from '../posicion';

@Component({
  selector: 'app-cubo',
  templateUrl: './cubo.component.html',
  styleUrls: ['./cubo.component.css']
})
export class CuboComponent implements OnInit {

	private rotateY : number;
	private rotateX : number;
	private column : number;
	private row : number;
  private transition : number;
	private cubo : Cubo;
	private listaMovimiento : Movimiento[];

  constructor(private service: CuboService) {
    this.service.savePosicion(-20,45, 1);
  	this.rotateY = this.service.readPosicion().y;
  	this.rotateX = this.service.readPosicion().x;
  	this.column = 1;
  	this.row = 1;
    this.transition = 0;
  	this.cubo = this.service.cuboRead();
  	this.listaMovimiento = this.service.readMovimiento();
  }

  @HostListener('document:keydown', ['$event']) 
  flechas(event: KeyboardEvent) {
    let code = event.keyCode;
    if (code === 37 || code === 38 || code === 39 || code === 40){
      this.transition = 0;
      this.moverCubo(event);
    }
    else if(code === 76 || code === 66 || code === 82 || code === 70 || code === 68 || code === 84){
      this.moveCara(code);
      this.service.savePosicion(this.rotateX, this.rotateY, 0);
    }
  }

  ngOnInit() {
  }
  	derecha(){
      if (String(this.row) != "null") {
    		this.service.derecha(this.row);
        this.service.guardarMovimiento("izquierda", this.row);
      }
  	}
  	izquierda() {
      if (String(this.row) != "null") {
    		this.service.izquierda(this.row);
        this.service.guardarMovimiento("derecha", this.row);
      }
  	}

  	abajo(){
      if (String(this.column) != "null") {
    		this.service.abajo(this.column);
        this.service.guardarMovimiento("arriba", this.column);
      }
  	}
  	arriba(){
      if (String(this.column) != "null") {
    		this.service.arriba(this.column);
        this.service.guardarMovimiento("abajo", this.column);
      }
  	}

  	rotarCara(cara: number) {
  		this.service.rotarCara(cara);
  	}

  	back() {
  		this.service.backMove();
  		this.service.saveCubo();
      this.cubo = this.service.cuboRead();
  	}

    mess(){
      let cont = 0;
      while (cont < 10){
        let x = Math.floor(Math.random() * 6) + 1;
        this.row = Math.floor(Math.random() * 3) + 1;
        this.column = Math.floor(Math.random() * 3) + 1;
        if (x === 1) {
          this.derecha();
        }
        else if (x === 2) {
          this.izquierda();
        }
        else if (x === 3) {
          this.arriba();
        }
        else if (x === 4) {
          this.abajo();
        }
        else if (x === 5) {
          this.service.arribaVerde(this.column);
          this.service.guardarMovimiento("abajoVerde", this.column);
        }
        else if (x === 6) {
          this.service.abajoVerde(this.column);
          this.service.guardarMovimiento("arribaVerde", this.column);
        }
        cont++;
      }
      this.column = 1;
      this.row = 1;
    }
    moveCara(code: number){
      if (code === 76){
      this.transition = 2;
      this.rotateX = -20;
      this.rotateY = 133;
      }
      else if (code === 66){
        this.transition = 2;
        this.rotateX = -20;
        this.rotateY = 225.5;
      }
      else if (code === 82){
        this.transition = 2;
        this.rotateX = -20;
        this.rotateY = -44;
      }
      else if (code === 70){
        this.transition = 2;
        this.rotateX = -20;
        this.rotateY = 45;
      }
      else if (code === 84){
        this.transition = 2;
        this.rotateX = -60;
        this.rotateY = 45;
      }
      else if (code === 68){
        this.transition = 2;
        this.rotateX = 60;
        this.rotateY = 45;
      }
    }
  	
	  moverCubo(event) {
      if (event.keyCode === 37) {
        this.rotateY -= 4;
        this.service.savePosicion(this.rotateX, this.rotateY, 0);
      }
      else if(event.keyCode === 39) {
        this.rotateY += 4;
        this.service.savePosicion(this.rotateX, this.rotateY, 0);
      }
      else if (event.keyCode === 38) {
        this.rotateX += 4;
        this.service.savePosicion(this.rotateX, this.rotateY, 0);
      }
      else if(event.keyCode === 40) {
        this.rotateX -= 4;
        this.service.savePosicion(this.rotateX, this.rotateY, 0);
      }
	  }

	rotate() {
		return 'rotateX(' + this.rotateX + 'deg) rotateY(' + this.rotateY + 'deg)';
	}

	reset() {
		this.service.resetCubo();
		this.cubo = this.service.cuboRead();
    this.listaMovimiento = this.service.readMovimiento();
		this.rotateY = 45;
  	this.rotateX = -20;
    this.service.savePosicion(this.rotateX, this.rotateY, 0);
	}

  transicionCubo(){
    return this.transition +'s';
  }

  resetPosicion(){
    this.transition = 2;
    this.rotateY = 45;
    this.rotateX = -20;
    this.service.savePosicion(this.rotateX, this.rotateY, 0);
    
  }

	validar() {
		return this.service.validar();
	}

  validarNum(event) {
    if((event.keyCode === 49 || event.keyCode === 50 || event.keyCode === 51)
      && (String(this.column) === "null" || String(this.row) === "null")){
      return true;
    }
    return false;
  }
}
