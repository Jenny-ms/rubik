export enum Sides {
	Front = "Front",
  	Top = "Top",
  	Bottom = "Bottom",
  	Left = "Left",
  	Right = "Right",
  	Back = "Back"
}
