import { Injectable } from '@angular/core';
import { Cara } from './cara';
import { Celda } from './celda';
import { Cubo } from './cubo';
import { Movimiento } from './movimiento';
import { Color } from './color.enum';
import { Posicion } from './posicion';
import { Sides } from './sides.enum';
@Injectable({
  providedIn: 'root'
})
export class CuboService {

	private cubo: Cubo;
	private listaMovimiento : Movimiento[];
  constructor() { 
  	this.cubo = this.llenarLista("");
  	this.listaMovimiento = this.readMovimiento();
  }

    derecha(fila: number){
    	let row = fila - 1;
    	let uno = this.cubo.caras[3].celdas[row];
  		let dos = this.cubo.caras[0].celdas[row];	
  		let tres = this.cubo.caras[1].celdas[row];
  		let cuatro = this.cubo.caras[2].celdas[row];

  		this.cubo.caras[0].celdas[row] = uno;
  		this.cubo.caras[1].celdas[row] = dos;
  		this.cubo.caras[2].celdas[row] = tres;
  		this.cubo.caras[3].celdas[row] = cuatro;

  		if (fila == 1) {
  			this.rotarCara(4);
  		}
      else if(fila == 3) {
  			for (var i = 0; i < 3; ++i) {
    				this.rotarCara(5);
    			}
  		}
      this.updateCara();
      this.saveCubo();
  	}
  	izquierda(row: number) {
  		for (var i = 0; i < 3; ++i) {
  			this.derecha(row);
  		}
  	}

  	abajo(column: number) {
  		for (var i = 0; i < 3; ++i) {
  			this.arriba(column);
  		}
  	}
  	arriba(column: number) {
  		let col = column - 1;
  		let col2 = 0;
  		if (col == 0) {
  			col2 = 2;
  		}
  		else if (col == 1) {
  			col2 = 1;
  		}
  		let uno : Celda[] = [];
  		uno.push(this.cubo.caras[5].celdas[0][col]);
  		uno.push(this.cubo.caras[5].celdas[1][col]);
  		uno.push(this.cubo.caras[5].celdas[2][col]);

  		let dos : Celda[] = [];
  		dos.push(this.cubo.caras[0].celdas[0][col]);
  		dos.push(this.cubo.caras[0].celdas[1][col]);
  		dos.push(this.cubo.caras[0].celdas[2][col]);

  		let tres : Celda[] = [];
  		tres.push(this.cubo.caras[4].celdas[2][col]);
  		tres.push(this.cubo.caras[4].celdas[1][col]);
  		tres.push(this.cubo.caras[4].celdas[0][col]);

  		let cuatro : Celda[] = [];
  		cuatro.push(this.cubo.caras[2].celdas[2][col2]);
  		cuatro.push(this.cubo.caras[2].celdas[1][col2]);
  		cuatro.push(this.cubo.caras[2].celdas[0][col2]);

  		this.cubo.caras[0].celdas[0][col] = uno[0];
  		this.cubo.caras[0].celdas[1][col] = uno[1];
  		this.cubo.caras[0].celdas[2][col] = uno[2];

  		this.cubo.caras[4].celdas[0][col] = dos[0];
  		this.cubo.caras[4].celdas[1][col] = dos[1];
  		this.cubo.caras[4].celdas[2][col] = dos[2];

  		this.cubo.caras[2].celdas[0][col2] = tres[0];
  		this.cubo.caras[2].celdas[1][col2] = tres[1];
  		this.cubo.caras[2].celdas[2][col2] = tres[2];

  		this.cubo.caras[5].celdas[0][col] = cuatro[0];
  		this.cubo.caras[5].celdas[1][col] = cuatro[1];
  		this.cubo.caras[5].celdas[2][col] = cuatro[2];

  		if (column == 1) {
  			this.rotarCara(3);
  		}
      else if(column == 3){
  			for (var i = 0; i < 3; ++i) {
    				this.rotarCara(1);
    		}
  		}
      this.updateCara();
      this.saveCubo();
  	}
    abajoVerde(column : number){
      for (var i = 0; i < 3; ++i) {
        this.arribaVerde(column);
      }
    }
    arribaVerde(column : number) {
      let col = 0;
      let col2 = 2; 
      if(column == 2){
        col = 1;
        col2 = 1;
      }
      else if(column == 3){
        col = 2;
        col2 = 0;
      }
      let uno = this.cubo.caras[5].celdas[col2];

      let dos : Celda[] = [];
      dos.push(this.cubo.caras[3].celdas[2][col]);
      dos.push(this.cubo.caras[3].celdas[1][col]);
      dos.push(this.cubo.caras[3].celdas[0][col]);

      let tres = this.cubo.caras[4].celdas[col];

      let cuatro : Celda[] = [];
      cuatro.push(this.cubo.caras[1].celdas[2][col2]);
      cuatro.push(this.cubo.caras[1].celdas[1][col2]);
      cuatro.push(this.cubo.caras[1].celdas[0][col2]);

      this.cubo.caras[3].celdas[0][col] = uno[0];
      this.cubo.caras[3].celdas[1][col] = uno[1];
      this.cubo.caras[3].celdas[2][col] = uno[2];

      this.cubo.caras[4].celdas[col] = dos;

      this.cubo.caras[1].celdas[0][col2] = tres[0];
      this.cubo.caras[1].celdas[1][col2] = tres[1];
      this.cubo.caras[1].celdas[2][col2] = tres[2];

      this.cubo.caras[5].celdas[col2] = cuatro;
      if (column === 1) {
        this.rotarCara(2);
      }
      else if (column === 3) {
        for (var i = 0; i < 3; ++i) {
            this.rotarCara(0);
        }
      }
      this.updateCara();
      this.saveCubo();
    }

  	rotarCara(cara: number) {
  		let uno : Celda[] = [];
  		uno.push(this.cubo.caras[cara].celdas[0][2]);
  		uno.push(this.cubo.caras[cara].celdas[1][2]);
  		uno.push(this.cubo.caras[cara].celdas[2][2]);

  		let dos = this.cubo.caras[cara].celdas[0];

  		let tres : Celda[] = [];
  		tres.push(this.cubo.caras[cara].celdas[0][0]);
  		tres.push(this.cubo.caras[cara].celdas[1][0]);
  		tres.push(this.cubo.caras[cara].celdas[2][0]);

  		let cuatro = this.cubo.caras[cara].celdas[2];

  		this.cubo.caras[cara].celdas[0] = uno;

  		this.cubo.caras[cara].celdas[1][0] = dos[1];

  		this.cubo.caras[cara].celdas[2] = tres;

  		this.cubo.caras[cara].celdas[1][2] = cuatro[1];
  	}

	llenarLista(funcion : string) {
		if (localStorage.getItem('cubo') === null || funcion === "reset") {
			let listaCaras : Cara[] = [];
			let caraFrente = new Cara(Color.Yellow, Sides.Front);
			listaCaras.push(caraFrente);
			let caraDerecha = new Cara(Color.Blue, Sides.Right);
			listaCaras.push(caraDerecha);
			let caraFondo = new Cara(Color.White, Sides.Back);
			listaCaras.push(caraFondo);
			let caraIzquierda = new Cara(Color.Green, Sides.Left);
			listaCaras.push(caraIzquierda);
			let caraArriba = new Cara(Color.Red, Sides.Top);
			listaCaras.push(caraArriba);
			let caraAbajo = new Cara(Color.Orange, Sides.Bottom);
			listaCaras.push(caraAbajo);
			let cuboTemp : Cubo = new Cubo(listaCaras);
			localStorage.setItem('cubo', JSON.stringify(cuboTemp));
			return cuboTemp;
		}
		else {
			return JSON.parse(localStorage.getItem('cubo') || '[]');
		}	
	}

	saveCubo() {
		localStorage.setItem('cubo', JSON.stringify(this.cubo));
	}

	cuboRead(){
		return this.cubo;
	}

	validar(){
  		for (var i = 0; i < this.cubo.caras.length; ++i) {
  			let color = "";
  			for (var j = 0; j < this.cubo.caras[i].celdas.length; ++j) {
  				for (var k = 0; k < this.cubo.caras[i].celdas[j].length; ++k) {
  					if (color == ""){
  						color = this.cubo.caras[i].celdas[j][k].color;
  					}
  					if (color != this.cubo.caras[i].celdas[j][k].color) {
  						return "Incompleto";
  					}
  					color = this.cubo.caras[i].celdas[j][k].color;
  				}
  			}
  		}
  		return "Completo";
  	}

  	resetCubo(){
  		this.cubo = this.llenarLista("reset");
      this.resetMovimiento();
  	}

    resetMovimiento() {
      this.listaMovimiento = [];
      localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
    }
  	guardarMovimiento(dir : string , pos : number) {
      let move : Movimiento = new Movimiento(dir,pos);
      this.listaMovimiento.push(move);
  		localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
  	}

  	readMovimiento() {
  		this.listaMovimiento = JSON.parse(localStorage.getItem('movimiento') || '[]');
    	return this.listaMovimiento;
  	}

  	backMove() {
      if (this.listaMovimiento.length > 0) {
    		let move : Movimiento = this.listaMovimiento[this.listaMovimiento.length-1];
    		if(move.direccion == "arriba") {
    			this.arriba(move.posicion);
    		}
    		else if(move.direccion == "abajo") {
    			this.abajo(move.posicion);
    		}
    		else if(move.direccion == "izquierda") {
    			this.izquierda(move.posicion);
    		}
        else if(move.direccion == "arribaVerde") {
          this.arribaVerde(move.posicion);
        }
        else if(move.direccion == "abajoVerde") {
          this.abajoVerde(move.posicion);
        }
    		else {
    			this.derecha(move.posicion);
    		}
    		this.listaMovimiento.splice(this.listaMovimiento.length-1 ,1);
    		localStorage.setItem('movimiento', JSON.stringify(this.listaMovimiento));
      }
  	}
    updateCara() {
      for (var i = 0; i < this.cubo.caras.length; ++i) {
        for (var j = 0; j < this.cubo.caras[i].celdas.length; ++j) {
          for (var k = 0; k < this.cubo.caras[i].celdas[j].length; ++k) {
            if (i === 0) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Front;
            }
            else if (i === 1) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Right;
            }
            else if (i === 2) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Back;
            }
            else if (i === 3) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Left;
            }
            else if (i === 4) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Top;
            }
            else if (i === 5) {
              this.cubo.caras[i].celdas[j][k].cara = Sides.Bottom;
            }
          } 
        }
      }
    }
    savePosicion(x :number , y: number, accion: number) {
      if (localStorage.getItem('posicion') === null || accion === 0){
        let posicion : Posicion = new Posicion(x, y);
        localStorage.setItem('posicion', JSON.stringify(posicion));
      }
      
    }
    readPosicion() {
      let posicion : Posicion = JSON.parse(localStorage.getItem('posicion') || '[]');
      return posicion;
    }
}
