import { Component, OnInit, Input } from '@angular/core';
import { Cara } from '../cara';
@Component({
  selector: 'app-cara',
  templateUrl: './cara.component.html',
  styleUrls: ['./cara.component.css']
})
export class CaraComponent implements OnInit {
	
	@Input() cara : Cara[];
  constructor() {
  }

  ngOnInit() {

  }

}
