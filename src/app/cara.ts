import { Celda } from './celda';

export class Cara {

	celdas : Celda[][];
	constructor(color : string, cara : string) {
		this.celdas = [];
		for (var i = 0; i < 3; ++i) {
			this.celdas[i] = [];
			for (var j = 0; j < 3; ++j) {
				this.celdas[i][j] = new Celda(color, cara);			
			}
		}
	}
}
