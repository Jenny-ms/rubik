export class Movimiento {
	direccion: string;
	posicion: number;

	constructor(direccion, posicion){
		this.direccion = direccion;
		this.posicion = posicion;
	}
}
